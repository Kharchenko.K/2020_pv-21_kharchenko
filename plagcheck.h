#ifndef CHECKPLAG_H
#define CHECKPLAG_H
#include <QString>
#include <QFile>
#include <iostream>
#include <vector>
#include <map>
using namespace std;

class PlagExcep:public exception
{
private:
    QString message;
public:
    PlagExcep(QString what):message(what){}
    QString what() {return message;}
};

template <class T>
class SmartPtr
{
    T* ptr;
public:
    SmartPtr():ptr(NULL){}
    ~SmartPtr(){if (ptr != NULL) delete ptr;}
    SmartPtr(T* obj): ptr(obj) {}
    SmartPtr(SmartPtr& obj):ptr(obj.ptr) {}
    SmartPtr& operator=(const SmartPtr& right){
        this->ptr = right.ptr;
        return *this;
    }
    T* operator->() const {return ptr;}
    T& operator*() const {return *ptr;}
    T* getPtr() const {return ptr;}
};

class PlagCheck
{
private:
    vector<QString> sample;
public:
    PlagCheck(QString& fname){read_sample(fname);}
    void read_sample(QString&);
    void read_text(QString&);
    bool isplag(QString&, map <QString, unsigned>&);
};

#endif // CHECKPLAG_H
