#include "mainwindow.h"
#include "plagcheck.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QString text;
    QString fname = ui->path->toPlainText().trimmed();
    SmartPtr<map<QString, unsigned>> res(new map <QString, unsigned>);
    try {
        text = ui->input->toPlainText();
        if(text.size()==0) throw exception();
        PlagCheck plag(fname);
        if(!plag.isplag(text, *res)){
            ui->output->setText("Ого! 100% уникальность!");
        }
        else{
            for(auto w: *res){
                if (w.second) ui->output->append(w.first + "-" + QString::number(w.second));
            }
        }
    }
    catch(PlagExcep& excep){
        QMessageBox::critical(this, "", excep.what());
    }
    catch(exception){
        QMessageBox::information(this, "", "Введите исходный текст");
    }
}
