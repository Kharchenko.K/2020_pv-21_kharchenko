#include "plagcheck.h"
#include <QMessageBox>

void PlagCheck::read_sample(QString & fname)
{
    //Как будто считываю, да
//    QString fpath("/home/konst/Documents/Qt/Lab10/Plag.txt");
    QFile file(fname);
    if (file.open(QIODevice::ReadOnly)){
        while(!file.atEnd())
            sample.push_back(file.readLine().trimmed());
        file.close();
    }
    else {
        throw PlagExcep("Не удалось открыть файл");
    }
}

bool PlagCheck::isplag(QString &text, map<QString, unsigned int> &res)
{
    bool f = 0;
    QStringList words, strings;
    QString word;
    strings = text.split("\n");
    while(strings.size()!=0)
        words += strings.takeFirst().toLower().split(QRegExp("[ .,?]+"));
    while(words.size() != 0){
        word = words.takeFirst();
        for(int i = 0; i < sample.size(); i++){
            if (word == sample[i].toLower()){
                f = 1; res[sample[i]]++;
            }
        }
    }
    return f;
}
